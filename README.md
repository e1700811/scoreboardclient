# Scoreboard
React project displaying data fetched from related api at http://e1700811scoreboard.herokuapp.com/
## Installation
1. Clone the project
2. run npm install
3. Enjoy!

## Features
1. Searching players by name.
2. Fetching all players scores in specified year.
3. Filtering and sorting data anyway you can imagine!

## Contribution
Project is free for anyone to modify, copy or break anytime they want