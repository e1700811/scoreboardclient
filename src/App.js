import React from 'react';
import Scoreboard from './Scoreboard';
import Search from './Search';

function App() {
  let params = new URLSearchParams(window.location.search);
	let nameSearch = params.get('name');
	let yearSearch = params.get('year');
  return (
    <div className="App">
      <Search nameSearch={nameSearch} yearSearch={yearSearch} />
      <Scoreboard nameSearch={nameSearch} yearSearch={yearSearch} />
    </div>
  );
}

export default App;
