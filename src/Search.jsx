import React from "react";
import './App.css';

const Search = props => {
	return (
		<div className="row">
			<form className="col-4">
				<label className="col-12" htmlFor="search-name">Search by player name</label>
				<input className="search-field" type="text" id="search-name" name="name" value={props.nameSearch} />
				<button className="search-button" type="submit">Search</button>
			</form>
			<form className="col-4">
				<label className="col-12" htmlFor="search-year">Search by Season</label>
				<input className="search-field" type="text" id="search-year" name="year" value={props.yearSearch} />
				<button className="search-button" type="submit">Search</button>
			</form>
			<form className="col-4">
				<label className="col-12" htmlFor="search-reset">Reset</label>
				<button type="submit" id="search-reset">Show all</button>
			</form>
		</div>
	);
};

export default Search;