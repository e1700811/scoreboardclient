import React, { useState, useEffect } from "react";
import ReactTable from "react-table-v6";
import "react-table-v6/react-table.css";
import './App.css';

const Scoreboard = props => {
	const [scoreboard, setScoreboard] = useState([]);
	let fetchUrl = 'http://e1700811scoreboard.herokuapp.com/scores';
	if (props.nameSearch) {
	fetchUrl += 'ByPlayer/' + props.nameSearch;
	}
	if (props.yearSearch) {
	fetchUrl += 'ByYear/' + props.yearSearch;
	}
	useEffect(() => {
		fetch(fetchUrl, {
			method: 'GET',
			headers: {
				'Accept': 'application/json',
				'Authorization' : 'basic dXNlcjpwYXNzd29yZDEyMw=='
			}
		}).then(function (response) {
				response.json().then(function (data) {
					setScoreboard(data);
				})
			})
	}, []);
	const columns = [
		{
			Header: 'Player',
			accessor: 'player.name',
		},
		{
			Header: 'Nationality',
			accessor: 'player.nationality',
		},
		{
			Header: 'Team',
			accessor: 'player.team',
		},
		{
			Header: 'Goals',
			accessor: 'goals',
		},
		{
			Header: 'Season',
			accessor: 'season',
		}
	];

	return (
		<div class="col-12">
			<ReactTable data={scoreboard} columns={columns} />
		</div>
	);
};

export default Scoreboard;